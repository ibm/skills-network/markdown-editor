import React from 'react';

interface Props {
  toggleModal: VoidFunction
}

const Header: React.FC<Props> = ({ toggleModal }: Props) => {
  return (
    <div className="header">
      <img 
        src="https://www.skills.network/wp-content/uploads/2019/06/IBM-Skills-Netwrok-Logo-Square.png"
        alt="logo" 
        style={{ maxWidth: 60 }}/>
      <h2>
        Markdown Preview
      </h2>
      <button className="btn" onClick={toggleModal}>Configure</button>
    </div>
  );
};

export default Header;
