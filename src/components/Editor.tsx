import React from 'react';

interface Props {
  src: string;
  handleSrcChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
}

const Editor: React.FC<Props> = ({ src, handleSrcChange }: Props) => {
  return (
    <div className="half-split">
      <textarea className="editor" value={src} onChange={handleSrcChange}/>
    </div>
  );
};

export default Editor;
