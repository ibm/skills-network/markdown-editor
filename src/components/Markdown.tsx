import React, { useContext, useEffect, useState } from 'react';
import ReactMarkdown from 'react-markdown';
import toc from 'remark-toc';
import CodeBlock from './CodeBlock';
import processLinkify from './../linkify';

import { configContext } from './../App';

interface Props {
  src: string;
}

const Markdown: React.FC<Props> = ({ src }: Props) => {
  const [processedSrc, setProcessedSrc] = useState<null | string>(null);
  const { prefixUrl, projectName, projectNamespace  } = useContext(configContext);

  useEffect(() => {
    const process = async () => {
      const res = await processLinkify(src, '', prefixUrl, '', projectName, projectNamespace);
      setProcessedSrc(res);
    };

    process();
  }, [src, prefixUrl, projectNamespace, projectName]);

  return (
    <div className="half-split markdown">
      <ReactMarkdown className="result" source={processedSrc ?? ''} plugins={[toc]} renderers={{ code: CodeBlock }}/>
    </div>
  );
};

export default Markdown;
