import React, { useCallback, useContext } from 'react';

import { configContext } from './../App';

interface Props {
  toggleModal: VoidFunction
}

const Modal: React.FC<Props> = ({ toggleModal }: Props) => {
  const { setConfig, projectName, projectNamespace, prefixUrl } = useContext(configContext);
  const handleChange = useCallback((newConfig: object) => {
    setConfig!({
      projectName,
      projectNamespace,
      prefixUrl,
      ...newConfig,
    });
  }, [prefixUrl, setConfig, projectName, projectNamespace]);

  return(
    <div className="modal">
      <div className="modal-body">

      <h3>Configure Markdown</h3>
        <div className="row">
          <input value={prefixUrl} onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChange({ prefixUrl: e.target.value })}/>Prefix URL
        </div>
        <div className="row">
          <input value={projectName} onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChange({ projectName: e.target.value })}/>Project Name
        </div>
        <div className="row">
          <input value={projectNamespace} onChange={(e:  React.ChangeEvent<HTMLInputElement>) => handleChange({ projectNamespace: e.target.value })}/>Project Namespace
        </div>
        <div className="row" style={{ justifyContent: 'center' }}>
          <button className="btn" onClick={toggleModal}>Close</button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
