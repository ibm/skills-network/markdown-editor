import React, { useEffect, useState } from 'react';
import hljs from 'highlight.js';

interface Props {
  language: string;
  value: string;
}

const CodeBlock: React.FC<Props> = ({ language, value }: Props) => {
  const [el, setEl] = useState<HTMLElement | null>(null);
  
  useEffect(() => {
    if(el){
      hljs.highlightBlock(el);
    }
  }, [value, language, el]);

  const setRef = (el: React.SetStateAction<HTMLElement | null>) => {
    setEl(el);
  };

  return (
    <>
      <pre>
        <code ref={setRef} className={language}>
          {value}
        </code>
      </pre>
    </>
  );
};

export default CodeBlock;
