import React, { createContext, useCallback, useEffect, useState } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import './App.css';
import Editor from './components/Editor';
import Header from './components/Header';
import Markdown from './components/Markdown';
import Modal from './components/Modal';

import queryString from 'query-string';

interface Config {
  projectName?: string
  prefixUrl?: string
  projectNamespace?: string
}

type ConfigContext = Config & {  
  setConfig?: Function }

const defaultValue: ConfigContext = {
  projectName: undefined,
  prefixUrl: undefined,
  projectNamespace: undefined,
  setConfig: () => {},
};

export const configContext = createContext<ConfigContext>(defaultValue);

const App = () => {
  const [src, setSrc] = useState(``);
  const [config, setConfig] = useState<Config>(defaultValue as Config);
  const [displayModal, setDisplayModal] = useState<Boolean>(false);

  useEffect(() => {
    const { prefixUrl, projectNamespace, projectName } = queryString.parse(window.location.search);
    setConfig({ prefixUrl: prefixUrl as string, projectNamespace: projectNamespace as string, projectName: projectName as string });
  }, []);

  const handleSrcChange = useCallback((event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setSrc(event.target.value);
  }, [setSrc]);

  const toggleModal = useCallback(() => {
    setDisplayModal(!displayModal);
  }, [displayModal, setDisplayModal]);

  return(
    <Router>
      <Route path="/">
        <configContext.Provider value={{ ...config, setConfig }}>
        {displayModal && <Modal toggleModal={toggleModal}/>}
        <Header toggleModal={toggleModal}/>
        <div className="container">
          <Editor src={src} handleSrcChange={handleSrcChange}/>
          <Markdown src={src}/>
        </div>
        </configContext.Provider>
      </Route>
    </Router>
  );
};

export default App;
