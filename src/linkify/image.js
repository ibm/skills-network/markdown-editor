import path from 'path';
import pathParse from 'path-parse';
import isRelativeUrl from 'is-relative-url';
import removeLeadingSlash from 'remove-leading-slash';
import urlJoin from 'url-join';

function linkify(imgNodes, filePath, baseUrl, inputUrlList = null) {
  const markdownFileDir = path.resolve(pathParse(filePath).dir);
  const rootDir = path.resolve('./');

  const urlList =
    inputUrlList ||
    imgNodes.reduce((acc, curr) => {
      const srcAttr = curr.attrs.find(eachAttr => eachAttr[0] === 'src');
      return acc.concat(srcAttr[1]);
    }, []);
  const uniqueUrlList = [...new Set(urlList)];
  const relativeUrlList = uniqueUrlList.filter(each => isRelativeUrl(each));

  const transformedUrlList = relativeUrlList.map(each => {
    const absolutePath = path.resolve(
      markdownFileDir,
      removeLeadingSlash(each)
    );
    const relativePath = path.relative(rootDir, absolutePath);
    return {
      content: each,
      absolute: absolutePath,
      transformed: encodeURI(urlJoin(baseUrl, relativePath)),
    };
  });

  console.log(transformedUrlList);
  return transformedUrlList;
}

const ImageTransformers = {
  linkify,
  supportedTags: ['img'],
  transform: (nodes, {markdownFilePath, baseUrl}) => {
    const urlList = linkify(nodes, markdownFilePath, baseUrl);
    return urlList;
  },
};

export default ImageTransformers;
