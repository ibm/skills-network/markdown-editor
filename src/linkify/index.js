
import Promise from 'promise';

// Unist
import unified from 'unified';
import visit from 'unist-util-visit';

// Remark
import parseMarkdown from 'remark-parse';
import stringifyMarkdown from 'remark-stringify';

import ImageTransformer from './image';
import {
  isFilteredUrl,
  noAutoLinkHostNameList,
  transformUrlToCampaignUrl,
  linkifyHtml,
} from './utils';

import dotenv from 'dotenv';
dotenv.config();


async function processLinkify(inputMarkdownString, 
  markdownFilePath = '',
  baseUrl = undefined,
  gitlabProjectId = undefined,
  gitlabProjectName = undefined,
  gitlabProjectNamespace = undefined) {
  gitlabProjectId = gitlabProjectId ?? (process.env.PROJECT_ID ?? '');
  gitlabProjectName = gitlabProjectName ?? (process.env.PROJECT_NAME ?? '');
  gitlabProjectNamespace = gitlabProjectNamespace ?? (process.env.PROJECT_NAMESPACE ?? '');
  baseUrl = baseUrl ?? (process.env.PREFIX ?? '');
  let category = '';
  if (gitlabProjectNamespace.indexOf('quicklabs') > -1) {
    category = 'SkillsNetwork-QuickLabs';
  }
  
  if (gitlabProjectNamespace.indexOf('courses') > -1) {
    category = 'SkillsNetwork-Courses';
  }
  
  function linkifyImgTags() {
    function transformer(tree) {
      visit(tree, 'image', node => {
        const transformedNodes = ImageTransformer.linkify(
          null,
          markdownFilePath,
          baseUrl,
          [node.url]
        );
        node.url =
          transformedNodes.length > 0
            ? transformedNodes[0].transformed
            : node.url;
        return visit.CONTINUE;
      });
    }
  
    return transformer;
  }
  
  // Linkify html as well
  function processRawHtml() {
    function transformer(tree) {
      visit(tree, 'html', node => {
        node.value = linkifyHtml(node.value, markdownFilePath, baseUrl);
        return visit.CONTINUE;
      });
    }
  
    return transformer;
  }
  
  // Convert some link nodes back to regular text node
  function normalizeLinkNodes() {
    function transformer(tree) {
      visit(tree, 'link', (node, index, parent) => {
        if (
          noAutoLinkHostNameList.map(r => r.test(node.url)).some(each => each)
        ) {
          parent.children.splice(index, 1, ...node.children);
          return [visit.SKIP, index];
        }
      });
    }
  
    return transformer;
  }
  
  function campaignifyUrl() {
    async function transformer(tree) {
      const linkNodes = [];
      visit(tree, 'link', node => {
        return linkNodes.push(node);
      });
      // Add campaign code to the rest of unfiltered link nodes
      await Promise.all(
        linkNodes.map(async node => {
          const isFiltered = await isFilteredUrl(node.url);
          if (!isFiltered) {
            node.url = transformUrlToCampaignUrl(
              node.url,
              [category, gitlabProjectName, gitlabProjectId].join('-')
            );
          }
        })
      );
    }
  
    return transformer;
  }

  const outputMarkdownVfile = await new Promise((resolve, reject) => {
    unified()
      .use(parseMarkdown)
      .use(linkifyImgTags)
      .use(processRawHtml)
      .use(stringifyMarkdown)
      .use(normalizeLinkNodes)
      .use(campaignifyUrl)
      .use(stringifyMarkdown, {gfm: false, fences: true})
      .process(inputMarkdownString, (err, file) => {
        if (err) reject(err);
        resolve(file);
      });
  });

  return String(outputMarkdownVfile);
}

export default processLinkify;
