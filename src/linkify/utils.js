import util from 'util';
import queryString from 'querystring';
import urlJoin from 'url-join';

// Unist
import unified from 'unified';
import visit from 'unist-util-visit';

// Rehype
import parseHtml from 'rehype-parse';
import stringifyHtml from 'rehype-stringify';

import { isPrivate } from 'url-is-private';

import ImageTransformer from './image';

const promisifyIsPrivate = util.promisify(isPrivate);

const noAutoLinkHostNameList = [
  /appdomain\.cloud/,
  /\.git$/,
  /localhost/,
  /((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)(\.(?!$)|$)){4}$/, // Matched ip address
];

const whitelist = [
  /appdomain\.cloud/,
  /github\.com/,
  /gitlab\.com/,
  /cocl\.us/,
  /\.git$/,
  /\$/,
  /localhost/,
];


function transformUrlToCampaignUrl(url, freeformDescriptor) {
  /* eslint-disable camelcase */
  const queryParams = queryString.stringify({
    cm_mmc: `Email_Newsletter-_-Developer_Ed+Tech-_-WW_WW-_-${freeformDescriptor}`,
    cm_mmca1: '000026UJ',
    cm_mmca2: '10006555',
    cm_mmca3: 'M12345678',
    cvosrc: 'email.Newsletter.M12345678',
    cvo_campaign: '000026UJ',
  });
  /* eslint-enable camelcase */

  return urlJoin(url, `?${queryParams}`);
}

function linkifyHtml(inputHtmlString, filePath, baseUrl) {
  const hast = unified()
    .use(parseHtml, {
      fragment: true,
    })
    .parse(inputHtmlString);

  let isModified = false;

  visit(
    hast,
    node => node.tagName === 'img',
    node => {
      const transformedNodes = ImageTransformer.linkify(
        null,
        filePath,
        baseUrl,
        [node.properties.src]
      );
      node.properties.src =
        transformedNodes.length > 0
          ? transformedNodes[0].transformed
          : node.properties.src;
      isModified = transformedNodes.length > 0;
      return visit.CONTINUE;
    }
  );

  return isModified
    ? unified()
        .use(stringifyHtml)
        .stringify(hast)
    : inputHtmlString;
}


/**
 * Return false for filtered url, true for accepted url
 * @param {string} url
 */
async function isFilteredUrl(url) {
  if (!url) return true;

  // Whitelist
  // A list of domains not to transform with our shortener service
  if (whitelist.map(r => r.test(url)).some(each => each)) return true;

  try {
    if (await promisifyIsPrivate(url)) return true;
  } catch (_) {
    // Gracefully handle error
    return true;
  }

  return false;
}

export {
  isFilteredUrl,
  noAutoLinkHostNameList,
  transformUrlToCampaignUrl,
  linkifyHtml,
};
