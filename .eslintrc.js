module.exports = {
  plugins: [
    "react"
  ],
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
  ],
  env: {
    browser: true,
  },
  globals: {
    process: true
  },
  settings: {
    react: {
      version: "latest",
    }
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: './tsconfig.json',
    sourceType: "module",
    ecmaVersion: 2018,
    ecmaFeatures: {
      jsx: true,
      modules: true,
      experimentalObjectRestSpread: true
    }
  },
  env: {
    es6: true,
    browser: true,
  },
  rules: {
    semi: "error",
    "eol-last": ["error", "always"]
  }
}
